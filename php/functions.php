<?php
	//funkcja ta, po wywołaniu tworzy tzw. sesję, czyli rejestrowane połączenie pomiędzy klientem i serwerem.
	//Podczas sesji można rejestrować dodatkowe informacje w nowej tablicy superzmiennych globalnych $_SESSION.
	//Informacje zapisywane w sesji giną po zamknięciu okna strony/zamknięciu przeglądarki. Jednak podczas
	//przechodzenia pomiędzy podstronami, podczas ładowania/przeładowywania strony WWW zmienne we wspomnianej
	//tablicy będą cały czas dostępne.
	//Zmienne możemy ustawiać lub kasować. Istotnym jest, że na każdej stronie bądź podstronie PHP (i tylko PHP)
	//musi znaleźć się odwołanie do tej funkcji celem rozpoczęcia lub KONTYNUOWANIA już zaczętej sesji. 
	session_start();

	$base;
	
	/*
	Funkcje w PHP nie różnią sięs specjalnie od funkcji z innych języków programowania (szczególnie od funkcji
	pisanych w JavaScript). Zasadniczą różnicą jest wskazane podawanie typu parametrów funkcji (od PHP 8).
	Poniższa funkjca pozwala na przyjęcie 4 parametrów, z czego jedynie jeden jest obowiązkowy. Pozostałe
	są nieobowiązkowe, a nie podanie ich powoduje przyjęcie wartości ustalonych przez twórcę funkcji:
	
	- dbname -  baza danych, której podanie jest wymagane od użytkownika. Możemy założyć, że bez niej
	nie odbędzie się połączenie do serwera SQL 
	
	- server - ciąg znakowy reprezentujący nazwę bądź adres serwera SQL 
	
	- login - nazwa użytkownika bazy danych 
	
	- pass - hasło użytkownika bazy danych
	
	Funkcja zwraca ciąg znakowy z informacją o udanym bądź nieudanym połączeniu.
	*/	
	function baseConnect(string $dbname, string $server='127.0.0.1', string $login='root', string $pass='') : string {
		$base = new mysqli($server, $login, $pass, $dbname);
		if (!$base)
			return "Nie udało się połączyć z bazą danych. Jeden lub więcej parametrów może być nieprawidłowych";
		else 
			return "Nawiązano połączenie.";
	}
	
	function baseClose() {
		$base->close();
	}	

	function loginToSite(string $login, string $pass) : string {
		$result = $base->query("SELECT id_osoba FROM `osoby` WHERE (`login`='$login' OR `email`='$logn') AND `haslo`=$pass;");
		if ($ret = $result->fetch_assoc()) {
			$_SESSION['id_osoba'];
			return "Zalogowano do systemu!";
						
		}
		return "Nie zalogowano do systemu";
	}


	function getIDTypes() : string {
		$baza = new mysqli('127.0.0.1', 'root', '', 'bankowa');
		$wynik = $baza->query("SHOW COLUMNS FROM `dowody_tozsamosci` WHERE `field`='dowod_tozsamosci';");
	/*	
		Poniższe linie kodu pokazują w jako sposób można obrobić tekst z typu enum celem stworzenia z niego listy,
		którą w następnym kroku będziemy chcieli wykorzystać jako bazę do listy w formularzu HTML
		$dzialanie1 = explode("(",$wynik->fetch_assoc()['Type']);
		print_r($dzialanie1);
		$dzialanie2 = substr($dzialanie1[1],0,-1);
		echo "<br/>";
		print_r($dzialanie2);
		$dzialanie3 = str_replace("'","", $dzialanie2);
		echo "<br/>";
		print_r($dzialanie3);
		$dzialanie4 = explode(',', $dzialanie3);
		echo "<br/>";
		print_r($dzialanie4);
	*/
		//linia poniżej wszystkie powyższe linie agreguje do jednej instrukcji złożonej
		return '[' . json_encode(explode(',',str_replace("'", "", substr(explode("(",$wynik->fetch_assoc()['Type'])[1],0,-1)))) . ']';
		
		//explode - funkcja pozwala na rozbicie pojedynczego ciągu znakowego na tablice ciągów znakowych podzielonych
		//względem określonego znaku bądź ciągu znaków. Przykład:
		//$ciag = "jabłko,cytryna,malina,gruszka";
		//$owoce = explode(',',$ciag); <- $owoce to teraz tablica z wartościami ['jabłko', 'cytryna', 'malina', 'gruszka']
		
		//substr - funkcja pozwala na pozyskanie z ciągu znakowe jego części (podciągu). Aby podzielić ciąg na części
		//możemy posługiwać się znacznikami liczbowymi start (opcjonalnie stop), gdzie znaczniki to po prostu numer 
		//miejsca (indeks) danego znaku w ciągu. Przykład:
		//$slowo = "Niedzielami";
		//$nowe_slowo = substr($slowo, 0, -2); <- $nowe_slowo będzie zawierać frazę "Niedziela" (będzie skopiowane wszystko
		//poza ostatnimi dwoma znakami)
		
		//str_replace - pozwala na zamianę określonego lub określonych znaków na inne. Przykład:
		//$ciag="Bazana";
		//$nowy_ciag = str_replace(['z'],['n'], $ciag); <- w efekcie $nowy_ciag będzie zawierał słowo "Banana"
		//
		//SHOW COLUMNS FROM `dowody_tozsamosci` WHERE `field`='dowod_tozsamosci';
	}

	