/*
	Poniżej drugi parametr funkcji to tzw. funkcja zwrotna (callback function). Ponieważ funkcja loadContent wykonuje swój kod w 
	nienormowanym czasie (po odwołaniu się do funkcji pobrania strony pozostały kod JS będzie się wykonywać niezależnie od tego,
	kiedy wykona się zdarzenie onreadystatechange (bo to jest zdarzenie, które wykonuje się w chwili jakichkolwiek zmian - w tym wypadku
	w chwili, gdy otrzymamy jakąkolwiek odpowiedź od serwera, do którego się odwołujemy). 
	Dlatego też po wywołaniu funkcji loadContent nie możemy dodać kodu, który zależałby od jej wyniku - bo nie wiemy, kiedy ten wynik się pojawi
	(czas odpowiedzi serwera, problemy z łączmi itp.)
	Dlatego w JS stosuje się własnie callback function. W poniższym wypadku taką funkcję reprezentuje drugi parametr naszej funkcji loadContent.
	Gdy wszytko w funkcji loadContent wykona się poprawnie i będzie ona kończyła swoje działanie (dla nas jest to chwila, gdy pozyskamy 
	odpowiedź od serwera - status 200), po prostu wypiszemy nazwę tego parametru z CO NAJMNIEJ zakończonego okrągłymni nawiasami
	(możemy też w te nawiasy wpisać dowolne wartości -> zmienne; jak do każdej innej wywoływanej funkcji).
	Spowoduje to wykonanie funkcji, która reprezentowana jest właśnie przez ten parametr.
*/
function loadContent(addressURL, post, callFunction) {
	/*
		W JavaScript występują trzy różne typy identyfikatorów dostępowych do wartości (w sensie stałych i zmiennych):
		- var - najpowszechniej używany; wskazuje JavaScript, że chcemy utworzyć zmienną o określonej nazwie
		        Zmienna ma zasięg życia obejmujący całą funkcję, w której została powołana do życia (np. gdyby xml był 
				typu var to od miejsca jego deklaracji można byłoby odwoływać się do niego w dowolnej linii kodu
				funkcji loadContent). Takie rozwiązanie może powodować problemy z reinterpretacją zmiennych bądź wyciekiem
				informacji poza przeznaczne miejsce
		- let - drugi rodzaj zmiennych w JavaScript. Zmienna ta jest ściśle związana z fragmentem kodu, w którym została powołana
		        do życia. Przykładowo jeżeli zostanie utworzona w pętli (for, while) lub warunku (if, if...else) to będzie ona
				dostępna jedynie w tych fragmentach kodu, zaś poza nimi zostanie ona automatycznie skasowana. Zwiększa tzw.
				hermetyzację (uściślenie dostępu do wartości), obecnie bardziej polecana dla funkcji
		- const - utworzenie stałej (nie zmiennej). Stałe tym odróżniają się od zmiennych, że po przypisaniu im podczas inicjalizacji
		          określonej wartości nie można jej zmienić w dalszej części kodu (może jedynie odwoływać się do tejże wartości).
				  Takie rozwiązanie jest pożądane jeżeli chcemy mieć pewność, że nie zmodyfikujemy zasobów, do których mamy przez tak utworzony
				  identyfikator dostęp. W przypadku efektywności - utworzenie stałej zamiast zmiennej lepiej organizuje pamięć
				  główną komputera (rezerwacja pamięci jest efektywniejsza dla stałych bo interpreter wie, jaka konkretna długość zostanie
				  zapisana). Nie zmienia to możliwości modyfikacji np. elementów obiektu, do którego się przez taką stałą odwołujemy, na przykład:
				  
				  const xml = new XMLHttpRequest()
				  xml = 12 //to spowoduje błąd - nie można zmienić wartości stałej!
				  xml.color = 12 //to nie spowoduje błędu. Chociaż modyfikujemy obiekt (tworzymy nową właściwość) to nie robimy tego bezpośrednio
								//na stałej, a na obiekcie, do którego stała jedynie prowadzi!
				  
	*/
	const xml = new XMLHttpRequest()
	xml.onreadystatechange = function() { 
								if (this.readyState === 4) {
									if (this.status === 200) {
										callFunction(this.responseText)
										//document.querySelector('#content').innerHTML = this.responseText
										//to co ma suę wykonać w przypadku sukcesu
									}
									else {
										console.log("Brak pliku w podanym adresie")
									}
								}
							}
	xml.open('POST', addressURL)
	xml.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
	xml.send(post)
	
}

function loadEvents(){
	var przyciskLogin = document.querySelector('#loginbuttonm')
	przyciskLogin.onclick = function(zdarzenie) {
								var login = document.querySelector('#loginform')
								//operator pozwala na dokonanie szybkiej, najczęściej jednoliniowej analizy
								//czy określony warunek zostanie spełniony. Jeżeli tak, to wykonuje się kod
								//od znaku zapytania do dwukropka. W przeciwnym wypadku wykopu wykona się kod
								//po dwukropku. Zaletą jest pominięcie if...else i możliwość przypisania samych
								//wartości (bez ciągłego przepisywania LEWEJ strony)
								login.style.height = (login.style.height==='') ? '60px' : ''
								document.querySelectorAll('.loginbuttonarrow').forEach( (wartosc) => {
																							if (wartosc.style.top==='') {
																							wartosc.style.transform='rotate3d(0,0,1,0deg)'
																							wartosc.style.top='5px'
																							}
																							else {
																								wartosc.style.transform=''
																								wartosc.style.top=''
																							}
																						})
						   }
	document.querySelector('#createaccount').onclick = function() {
															
															loadContent('./sites/register.html', '', function(returnValue) {
																document.querySelector('#content').innerHTML = returnValue
																loadContent('./php/control.php?action=getIDCards', '', function(ret) {
																	//console.log(ret)
																	//const aside = document.querySelector('main aside')
																	//aside.style.cssText = (aside.style.display==='') ? 'display: block; width: 20%;' : ''
																	//console.log(JSON.parse(ret)[0])
																	//aside.innerHTML = ret
																	const js = JSON.parse(ret)[0]
																	const sel = document.querySelector('#registerform select[name=dowod]')
																	js.forEach( (v,k) => {
																		let option = document.createElement('option')
																		option.value = k+1
																		option.innerText = v
																		//console.log(sel)
																		sel.appendChild(option)
																	})
																	sel.onchange = function() {
																		for(let i=0;i<sel.children.length;i++) 
																			if (sel.children[i].value==sel.value) {
																				document.querySelector('input[name=id_dowod_tozsamosc]').
																					placeholder = 'Numer ' + sel.children[i].innerText
																				break
																			}																		
																	}																		
																})
																const btns = document.querySelectorAll('#registerform button')
																btns[1].onclick = 
																			function () {
																				document.querySelectorAll('#registerform input').
																					forEach( function(v) {
																						//element dla elementów typu radio
																						if (v.type==='radio') v.checked=false
																						//dla wszystkich pozostałych elementów naszego formularza
																						else v.value=''
																						
																						//console.log(v.value)	
																					})
																			}
																btns[0].onclick = 
																			function () {
																				var postText=''
																				//zmienna wykorzystywana w forEach; opis jej zastosowania
																				//w forEach
																				var pass=''
																				const sel = document.querySelector('#registerform select')
																				postText+=`${sel.name}=${sel.value}&`
																				document.querySelectorAll('#registerform input').
																				forEach( function(v) {
																					/*
																					Wszystkie poniższe  warunki działają dokładnie tak samo. Różnią się
																					sposobem zapisu i konstrukcją warunków. Można korzystać z dowolnego z nich.
																					*/
																					//if (v.type!=='radio')
																					//	postText+=v.name+'='+v.value
																					//else {
																					//	if (v.checked)
																					//		postText+=v.name+'='+v.value
																					//}
																					
																					//if (v.type==='radio'&&v.checked || v.type!=='radio')
																					//	postText+=v.name+'='+v.value
																					
																					//poniższa linia wydaje się być najsensowniejszą implementacją warunku dodawania
																					//wartości formularza do przesyłu przez zmienne typu POST. Jednak można dokonać 
																					//pewnej modyfikacji...
																					//if (v.type==='radio'&&v.checked || v.type!=='radio')
																					//	postText+=`${v.name}=${v.value}`
																					
																					//JS czyta warunki od lewej do prawej. W związku z tym w pierwszej kolejności
																					//możemy zbadać taki warunek, któy najczęściej jest pewny. W naszym wypadku
																					//więcej jest pól tekstowych czy z hasłem (tekstowe) niż pól radio.
																					//Z kolei jeżeli pierwszy warunek się wykona negatywnie to jasnym jest, że typem 
																					//pola jest radio i nie ma potrzeby jeszcze raz tego badać. Dlatego sprawdzamy jedynie
																					//czy dany przycisk został zaznaczony. Jeżeli tak - dodajemy jego wartość do postText
																					if (v.type!=='radio' || v.checked)
																						postText+=`${v.name}=${v.value}&`
																					
																					//postText+=(v.type==='radio'&&v.checked || v.type!=='radio') ? `${v.name}=${v.value}` : ''
																					//postText+=(v.type!=='radio') ? v.name+'='+v.value : (v.checked) ? v.name+'='+v.value : '' 
																					/*
																					Celem profejonalnego przesyłania danych użytkownika do bazy danych
																					należy dodatkowo sprawdzić np. wprowadzone hasło. Ważne jest, by 
																					hasło w obu polach (wprowadzenia oraz ponownego wprowadzenia)
																					były identyczne. Jeżeli tak się nie stanie - użytkownik pownien
																					otrzymać stosowny komunikat.
																					
																					Poniżej tworzymy zmienną pass. Będzie ona przechowywac pierwszej
																					znalezione hasło (niezależnie od ułożenia pola hasło/powtórzenie
																					hasła w formularzu)
																					
																					PONIŻSZE ZAŁOŻENIE by w tej części kodu tworzyć zmienną to błąd.
																					Zmienna zostanie utworzona tylko dla POJEDYNCZEGO przebiegu 
																					funkcji w forEach. Pod koniec jej działania zostanie skasowana 
																					(var żyje do domknięcia klamry). W tym celu zmienna pass jest
																					zadeklarowana przed forEach
																					*/
																					//var pass=''
																					
																					/*
																						Poniżej sprawdzamy czy aktualnie skrypt wybrał dla nas 
																						pole typu password oraz czy zmienna pass ma wartość inną niż
																						pusta. Jeżeli tak oraz jeżeli zmienna pass zawiera identyczny
																						ciąg wartości jak pole v (jego wartość value) to stwierdzamy
																						zgodność hasła																					
																					*/
																					
																					if (v.type=='password' && pass!='' && (pass===v.value)) 
																						console.log('HASLA ZGODNE')
																					//jeżeli jednak zmienna pass nie została ustawiona to 
																					//a mamy do czynienia z polem password to ustwiamy wartość zmiennej
																					else if (v.type=='password' && pass=='')
																						pass = v.value
																					//jeżeli żaden wcześniejszy warunek nie został spełoniony
																					//tj. nie ustawialiśmy zmiennej, zaś wartości zmienna i pole były
																					//różne komunikujemy błąd hasła.
																					else  
																						console.log('HASLA NIEZGODNE')	
																				})
																				postText=postText.slice(0,-1)
																				loadContent('./php/personsmisc.php', postText,
																				function(rev) {
																					document.querySelector('#content').innerHTML = rev
																				})
																				//console.log(postText)
																			}
															})
													   }
}

