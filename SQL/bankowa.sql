-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 08 Lut 2022, 10:00
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bankowa`
--
CREATE DATABASE IF NOT EXISTS `bankowa` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `bankowa`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dowody_tozsamosci`
--

DROP TABLE IF EXISTS `dowody_tozsamosci`;
CREATE TABLE `dowody_tozsamosci` (
  `id_dowod_tozsamosci` bigint(20) UNSIGNED NOT NULL,
  `nr_identyfikacyjny` varchar(20) NOT NULL,
  `dowod_tozsamosci` enum('dowód osobisty','paszport','karta stałego pobytu') NOT NULL,
  `numer` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `dowody_tozsamosci`
--

INSERT INTO `dowody_tozsamosci` (`id_dowod_tozsamosci`, `nr_identyfikacyjny`, `dowod_tozsamosci`, `numer`) VALUES
(1, '', 'dowód osobisty', ''),
(2, '', 'karta stałego pobytu', ''),
(3, '', 'karta stałego pobytu', ''),
(4, '', 'karta stałego pobytu', ''),
(5, 'AAA', 'karta stałego pobytu', '564213'),
(6, 'AAA', 'karta stałego pobytu', '564213'),
(7, 'AAA', 'karta stałego pobytu', '564213'),
(8, 'AAA', 'karta stałego pobytu', '564213'),
(9, 'AAA', 'karta stałego pobytu', '564213'),
(10, 'AAA', 'karta stałego pobytu', '564213'),
(11, 'BBB', 'karta stałego pobytu', '564213'),
(12, 'AAA', 'dowód osobisty', '565656'),
(15, 'AAA', 'dowód osobisty', '565656');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci`
--

DROP TABLE IF EXISTS `klienci`;
CREATE TABLE `klienci` (
  `id_klient` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `nazwa` int(11) NOT NULL,
  `numer` int(11) NOT NULL COMMENT 'NIP/PESEL/INNY IDENTYFIKATOR PODMIOTU',
  `data_zalozenia` date NOT NULL,
  `aktywny` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci_konta_ror`
--

DROP TABLE IF EXISTS `klienci_konta_ror`;
CREATE TABLE `klienci_konta_ror` (
  `id_klient` bigint(20) UNSIGNED NOT NULL,
  `id_konto_ror` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `konta_ror`
--

DROP TABLE IF EXISTS `konta_ror`;
CREATE TABLE `konta_ror` (
  `id_konto_ror` bigint(20) UNSIGNED NOT NULL,
  `numer` char(26) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(30) NOT NULL COMMENT 'imię danej osoby',
  `nazwisko` varchar(50) NOT NULL,
  `id_dowod_tozsamosc` bigint(20) UNSIGNED NOT NULL,
  `plec` set('M','K','N') NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefon` varchar(18) NOT NULL,
  `login` varchar(10) NOT NULL,
  `haslo` char(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`id_osoba`, `imie`, `nazwisko`, `id_dowod_tozsamosc`, `plec`, `email`, `telefon`, `login`, `haslo`) VALUES
(13, 'Janina', 'Iksińska', 12, 'K', 'janka@wsp.pl', '777888999', 'jnaka', 'abcd');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dowody_tozsamosci`
--
ALTER TABLE `dowody_tozsamosci`
  ADD UNIQUE KEY `id_dowod_tozsamosci` (`id_dowod_tozsamosci`);

--
-- Indeksy dla tabeli `klienci`
--
ALTER TABLE `klienci`
  ADD UNIQUE KEY `id_klient` (`id_klient`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indeksy dla tabeli `klienci_konta_ror`
--
ALTER TABLE `klienci_konta_ror`
  ADD KEY `id_klient` (`id_klient`),
  ADD KEY `id_konto_ror` (`id_konto_ror`);

--
-- Indeksy dla tabeli `konta_ror`
--
ALTER TABLE `konta_ror`
  ADD UNIQUE KEY `id_konto_ror` (`id_konto_ror`);

--
-- Indeksy dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_dowod_tozsamosc` (`id_dowod_tozsamosc`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `dowody_tozsamosci`
--
ALTER TABLE `dowody_tozsamosci`
  MODIFY `id_dowod_tozsamosci` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `klienci`
--
ALTER TABLE `klienci`
  MODIFY `id_klient` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `konta_ror`
--
ALTER TABLE `konta_ror`
  MODIFY `id_konto_ror` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `klienci`
--
ALTER TABLE `klienci`
  ADD CONSTRAINT `klienci_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `klienci_konta_ror`
--
ALTER TABLE `klienci_konta_ror`
  ADD CONSTRAINT `klienci_konta_ror_ibfk_1` FOREIGN KEY (`id_klient`) REFERENCES `klienci` (`id_klient`),
  ADD CONSTRAINT `klienci_konta_ror_ibfk_2` FOREIGN KEY (`id_konto_ror`) REFERENCES `konta_ror` (`id_konto_ror`);

--
-- Ograniczenia dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD CONSTRAINT `osoby_ibfk_1` FOREIGN KEY (`id_dowod_tozsamosc`) REFERENCES `dowody_tozsamosci` (`id_dowod_tozsamosci`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
